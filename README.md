# Jarris Seamod #

This is a modification of conkyrc_seamod by SeaJey for the popular seamod conky plugin

## Screenshot ##
![conky_seamod.jpg](https://bitbucket.org/repo/LGroko/images/971012559-conky_seamod.jpg)

## Installation ##

If you do not have Conky installed, run this command in a terminal

```
#!shell

sudo apt-get install conky-all
```
Then run this to create the folder, then clone the files into it

```
#!shell

mkdir -p ~/.conky/JarrisSeamod
git clone git@bitbucket.org:DarkJarris/jarrisseamod.git ~/.conky/JarrisSeamod
```

Note: This assumes ~/.conky/JarrisSeamod. If not, correct script references in conkyrc_seamod file line 64


# Starting at Boot #

To make it autostart, you can use either of 2 methods:

### Crontab ###
```
#!shell


crontab -e
```

Adding a line like this to it:


```
#!shell

@reboot conky -c ~/.conky/JarrisSeamod/conkyrc_seamod
```

will execute the script once your computer boots up.


### GUI ###

System->Preferences->Startup Applications->Add

*Refer to your Desktop Environment equivalent if you cannot see it*

```
#!shell

conky -c ~/.conky/JarrisSeamod/conkyrc_seamod
```